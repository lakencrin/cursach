#pragma once

#include <stdint.h>

struct Index
{
	Index(int64_t x, int64_t y)
		: _x{ x }
		, _y{ y }
	{}
	Index()
		: _x{ (int64_t)-1 }
		, _y{ (int64_t)-1 }
	{}

	Index(const Index& other)
		: _x{ other.x() }
		, _y{ other.y() }
	{}

	int64_t x() const { return _x; }
	int64_t y() const { return _y; }

	bool isValid() const { return _x >= 0 and _y >= 0; }

	inline Index operator-(const Index& other) const
	{
		return Index{ x() - other.x(), y() - other.y() };
	}
	inline Index operator+(const Index& other) const
	{
		return Index{ x() + other.x(), y() + other.y() };
	}

	inline bool operator==(const Index& other) const
	{
		return x() == other.x() and y() == other.y();
	}
	inline bool operator!=(const Index& other) const
	{
		return x() != other.x() or y() != other.y();
	}

	inline Index& operator+=(const Index& other)
	{
		_x += other.x();
		_y += other.y();
		return *this;
	}

	inline Index& operator-=(const Index& other)
	{
		_x -= other.x();
		_y -= other.y();
		return *this;
	}

	inline Index& subX(int64_t x)
	{
		_x -= x;
		return *this;
	}
	inline Index& addX(int64_t x)
	{
		_x += x;
		return *this;
	}

	inline Index& subY(int64_t y)
	{
		_y -= y;
		return *this;
	}
	inline Index& addY(int64_t y)
	{
		_y += y;
		return *this;
	}

private:
	int64_t _x, _y;
};
