#include "CrosswordCreator.h"

#include <algorithm>
#include <cmath>
#include <iostream>


CrosswordCreator::CrosswordCreator(StringList list)
	: _words{ std::move(list) }
	, field{ size(_words) }
{
	init();
}

CrosswordCreator::CrosswordCreator(StringList list, int64_t x, int64_t y)
	: _words{ std::move(list) }
	, field{ x, y }
{
	init();
}

void CrosswordCreator::init()
{
	std::sort(_words.begin(), _words.end(), [](const std::string& left, const std::string& right) { return left.size() > right.size(); });
	std::for_each(_words.begin(), _words.end(), [](std::string& string) { std::transform(string.begin(), string.end(), string.begin(), std::toupper); });

	Index first_pos = field.placeToCenterHor(_words.front());
	for (auto& letter : _words.front())
	{
		_letters[letter].push_back(first_pos);
		first_pos.addX(1);
	}

	_words.erase(_words.begin());

	while (_words.size())
	{
		std::string& string = _words.front();

		if (not tryPlaceWord(string))
			unused.push_back(string);

		_words.erase(_words.begin());
	}

	while (not unused.empty())
	{
		bool used = false;
		for (int i = 0; i < unused.size(); ++i)
		{
			std::string& string = unused[i];

			if (tryPlaceWord(string))
			{
				used = true;
				unused.erase(unused.begin());
			}
		}

		if (not used)
			break;

	}
}

bool CrosswordCreator::tryPlaceWord(const std::string& string)
{
	bool flag = false;
	for (int64_t letter_index = 0; letter_index < string.size(); ++letter_index)
	{
		char const letter = string[letter_index];

		auto point = _letters.find(letter);
		if (point == _letters.end())
			continue;

		std::vector<Index>& indices = point->second;

		for (auto& index : indices)
		{
			try
			{
				if (not field.canBePlacedHorizontally(string, letter_index, index))
				{
					if (not field.canBePlacedVertically(string, letter_index, index))
						continue;
					else
					{
						Index pos = field.placeVertically(string, letter_index, index);
						fillMapVer(string, pos);
						flag = true;
						break;
					}
				}
				else
				{
					Index pos = field.placeHorizontally(string, letter_index, index);
					fillMapHor(string, pos);
					flag = true;
					break;
				}
			}
			catch (std::exception&) {}
		}
		if (flag)
			break;
	}

	return flag;
}


void CrosswordCreator::fillMapVer(const std::string& string, Index index)
{
	for (auto& letter : string)
	{
		_letters[letter].push_back(index);
		index.addY(1);
	}
}
void CrosswordCreator::fillMapHor(const std::string& string, Index index)
{
	for (auto& letter : string)
	{
		_letters[letter].push_back(index);
		index.addX(1);
	}
}

void CrosswordCreator::write(std::ostream& out)
{
	field.write(out);

	if (not unused.empty())
	{
		out << "Unused words:\n";
		for (auto& string : unused)
		{
			out << string << "\n";
		}
	}

	out << "====================================================\n\n";
}

int64_t CrosswordCreator::size(const StringList& list)
{
	int64_t char_count = 0;

	for (auto& string : list)
		char_count += string.size() - 1;

	return char_count;
}
