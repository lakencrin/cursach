#include "CrosswordCreator.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


int main(int argc, char* argv[])
{
	std::ifstream in{ "bigin.txt" };

	StringList words;
	while (!in.eof())
	{
		std::string word;
		in >> word;

		if (word == ".")
			break;

		words.push_back(word);
	}

	CrosswordCreator creator{ words, 40, 40 };

	creator.write(std::cout);


	return 0;
}
