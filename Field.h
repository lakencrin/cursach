#pragma once

#include "Index.h"

#include <vector>
#include <string>
#include <iostream>



class Field
{
	std::vector<std::vector<char>> array;

	const int64_t x, y;

	int64_t upper, lower;
	int64_t left, right;

public:
	Field(int64_t x, int64_t y)
		: x{ x }
		, y{ y }
	{
		init();
	}
	Field(int64_t size)
		: Field{ size, size }
	{
		init();
	}

	void init()
	{
		array.resize(y);
		for (auto& vector : array)
			vector.resize(x, '.');

		upper = y;
		lower = 0;

		left = x;
		right = 0;
	}

	Index placeToCenterHor(const std::string& string)
	{
		Index const start_pos = this->center() -= Index{ (string.size() - 1) / 2, 0 };
		
		if (not isInField(this->center() += Index{ string.size() / 2, 0 }) or not isInField(start_pos))
			throw std::exception{ "Out of range" };

		Index position = start_pos;

		for (auto& letter : string)
		{
			setLetter(letter, position);
			position.addX(1);
		}

		return start_pos;
	}

	Index placeHorizontally(const std::string& string, int64_t from, const Index& position)
	{
		Index const start_pos = position - Index{ from, 0 };
		Index const end_pos = start_pos + Index{ string.size() - 1, 0 };

		if (not canBePlacedHorizontally(string, from, position))
			throw std::exception{};

		Index pos = start_pos;
		for (auto& letter : string)
		{
			setLetter(letter, pos);
			pos.addX(1);
		}

		if (end_pos.x() > right)
			right = end_pos.x();

		if (start_pos.x() < left)
			left = start_pos.x();

		return start_pos;
	}

	Index placeVertically(const std::string& string, int64_t from, const Index& position)
	{
		Index const start_pos = position - Index{ 0, from };
		Index const end_pos = start_pos + Index{ 0, string.size() - 1 };
		
		if (not canBePlacedVertically(string, from, position))
			throw std::exception{};

		Index pos = start_pos;
		for (auto& letter : string)
		{
			setLetter(letter, pos);
			pos.addY(1);
		}

		if (end_pos.y() > lower)
			lower = end_pos.y();

		if (start_pos.y() < upper)
			upper = start_pos.y();

		return start_pos;
	}

	void write(std::ostream& out)
	{
		/*for (int64_t ver = upper; ver < lower; ++ver)
		{
			std::vector<char>& line = array[ver];
			for (int64_t hor = left; hor < right; ++hor)
			{
				std::string temp;
				temp += line[hor];
				temp += " ";
				out << temp;
			}
			out << "\n";
		}*/

		for (auto& line : array)
		{
			for (auto& letter : line)
			{
				std::string temp;
				temp += letter;
				temp += " ";
				out << temp;
			}
			out << "\n";
		}
	}

	inline Index center()
	{
		return Index{ x / 2, y / 2 };
	}

	bool isInField(const Index& index)
	{
		return index.isValid() and index.x() < x and index.y() < y;
	}

	bool isLettersBetweenHor(Index start, Index end, const Index& except)
	{
		while (start != end)
		{
			if (start == except)
			{
				start.addX(1);
				continue;
			}

			if (getLetter(start) != '.')
				return true;
			start.addX(1);
		}
		return false;
	}
	bool isLettersBetweenVer(Index start, Index end, const Index& except)
	{
		while (start != end)
		{
			if (start == except)
			{
				start.addY(1);
				continue;
			}

			if (getLetter(start) != '.')
				return true;
			start.addY(1);
		}
		return false;
	}

	bool canBePlacedHorizontally(const std::string& string, int64_t from, const Index& position)
	{
		Index const start_pos = position - Index{ from, 0 };
		Index const end_pos = start_pos + Index{ string.size() - 1, 0 };

		if (not isInField(start_pos) or not isInField(end_pos))
			return false;

		if (not checkForPlacementHor(string, from, start_pos, end_pos))
			return false;

		return true;
	}
	bool canBePlacedVertically(const std::string& string, int64_t from, const Index& position)
	{
		Index const start_pos = position - Index{ 0, from };
		Index const end_pos = start_pos + Index{ 0, string.size() - 1 };

		if (not isInField(start_pos) or not isInField(end_pos))
			return false;

		if (not checkForPlacementVer(string, from, start_pos, end_pos))
			return false;

		return true;
	}

	bool checkForPlacementHor(const std::string& string, int64_t from, Index start, Index end)
	{
		{
			Index index = start - Index{ 1, 0 };
			if (not isInField(index) or getLetter(index) != '.')
				return false;
		}
		{
			Index index = end + Index{ 1, 0 };
			if (not isInField(index) or getLetter(index) != '.')
				return false;
		}

		for (int64_t pos = 0; pos < string.size(); start.addX(1), ++pos)
		{
			char const letter = getLetter(start);
			if (letter == '.')
			{
				{
					Index index = start + Index{ 0, 1 };
					if (isInField(index) and getLetter(index) != '.')
						return false;
				}
				{
					Index index = start - Index{ 0, 1 };
					if (isInField(index) and getLetter(index) != '.')
						return false;
				}
			}
			else if (letter != string[pos])
				return false;
		}
		return true;
	}
	bool checkForPlacementVer(const std::string& string, int64_t from, Index start, Index end)
	{
		{
			Index index = start - Index{ 0, 1 };
			if (not isInField(index) or getLetter(index) != '.')
				return false;
		}
		{
			Index index = end + Index{ 0, 1 };
			if (not isInField(index) or getLetter(index) != '.')
				return false;
		}

		for (int64_t pos = 0; pos < string.size(); start.addY(1), ++pos)
		{
			char const letter = getLetter(start);
			if (letter == '.')
			{
				{
					Index index = start + Index{ 1, 0 };
					if (isInField(index) and getLetter(index) != '.')
						return false;
				}
				{
					Index index = start - Index{ 1, 0 };
					if (isInField(index) and getLetter(index) != '.')
						return false;
				}
			}
			else if (letter != string[pos])
				return false;
		}
		return true;
	}

	//bool checkSurroundingsHor(const std::string& string, int64_t from, Index start, Index end)
	//{
	//	/*{
	//		Index index = start - Index{ 1, 0 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//	}
	//	{
	//		Index index = end + Index{ 1, 0 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//	}
	//	Index const except = start + Index{ 0, from };
	//	while (start != end)
	//	{
	//		if (start == except)
	//			
	//		Index index = start + Index{ 0, 1 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//		index = start + Index{ 0, -1 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//		start.addX(1);
	//	}*/
	//	return true;
	//}
	//bool checkSurroundingsVer(const std::string& string, int64_t from, Index start_pos, Index end_pos)
	//{
	//	{
	//		Index index = start_pos - Index{ 0, 1 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//	}
	//	{
	//		Index index = end_pos + Index{ 0, 1 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//	}
	//	Index const except = start_pos + Index{ 0, from };
	//	while (start_pos != end_pos)
	//	{
	//		if (start_pos == except)
	//			continue;
	//		Index index = start_pos + Index{ 1, 0 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//		index = start_pos + Index{ -1, 0 };
	//		if (not isInField(index) or getLetter(index) != '.')
	//			return false;
	//		start_pos.addY(1);
	//	}
	//	return true;
	//}

	bool isWordHorizontally(Index pos)
	{
		return (getLetter(pos.subX(1)) != '.' and getLetter(pos.addX(1)) != '.');
	}
	bool isWordVertically(Index pos)
	{
		return (getLetter(pos.subY(1)) != '.' and getLetter(pos.addY(1)) != '.');
	}

	void setLetter(char letter, const Index& index)
	{
		array[index.y()][index.x()] = letter;
	}
	char getLetter(const Index& index)
	{
		return array[index.y()][index.x()];
	}
};

