#pragma once

#include "Index.h"
#include "Field.h"

#include <vector>
#include <string>
#include <ios>
#include <map>

using StringList = std::vector<std::string>;



class CrosswordCreator
{
	StringList _words;

	Field field;

public:
	CrosswordCreator(StringList list);
	CrosswordCreator(StringList list, int64_t x, int64_t y);
	~CrosswordCreator() = default;

	void write(std::ostream&);

	void init();

private:
	void fillMapVer(const std::string& string, Index index);
	void fillMapHor(const std::string& string, Index index);

	bool tryPlaceWord(const std::string& string);

	static int64_t size(const StringList& list);

	std::map<char, std::vector<Index>> _letters;
	StringList unused;
};

